import boto3
import json
from datetime import datetime, timedelta

ec2 = boto3.client('ec2')
cloudwatch = boto3.client('cloudwatch')
sns = boto3.client('sns')

def lambda_handler(event, context):
    # Get instance ID from event
    instance_id = event['detail']['responseElements']['instancesSet']['items'][0]['instanceId']

    # Get instance type and launch time
    instance = ec2.describe_instances(InstanceIds=[instance_id])['Reservations'][0]['Instances'][0]
    instance_type = instance['InstanceType']
    launch_time = instance['LaunchTime']

    # Wait for up to 5 minutes for CPU data to become available
    cpu_data = []
    end_time = datetime.utcnow()
    start_time = end_time - timedelta(minutes=25)
    while len(cpu_data) < 3 and start_time < launch_time + timedelta(minutes=5):
        response = cloudwatch.get_metric_statistics(
            Namespace='AWS/EC2',
            MetricName='CPUUtilization',
            Dimensions=[{'Name': 'InstanceId', 'Value': instance_id}],
            StartTime=start_time,
            EndTime=end_time,
            Period=300,
            Statistics=['Average']
        )
        cpu_data = response['Datapoints']
        start_time += timedelta(minutes=5)

    # Check if CPU utilization exceeded threshold
    if len(cpu_data) >= 3 and cpu_data[-1]['Average'] >= 90:
        # Send notification to SNS
        message = f"Instance {instance_id} (type {instance_type}) exceeded CPU threshold with average utilization of {cpu_data[-1]['Average']}% over the last 15 minutes."
        response = sns.publish(
            #receive TopicArn from os env variable
            TopicArn=os.environ['SNS_TOPIC_ARN'],
            Subject='EC2 CPU Alarm',
            Message=message
        )
        print(f"Sent SNS message with message ID {response['MessageId']}")
    else:
        print(f"CPU utilization for instance {instance_id} (type {instance_type}) is within limits.")
