# VescapeDemo

A demo for the Vescape project that shows how to integrate different cloud functions into a single application.

## Getting Started

Call manually the Gitlab CI pipeline to deploy the application on the cloud.

## Prerequisites

It is mandatory to provide AWS credentials to the Gitlab CI pipeline.

## Installing

When deploying the application, an email confirming the subscription will be sent to the user. The email contains a link to confirm the subscription. The link is valid for 24 hours.

## Deployment

![Flow](./diagram.png)
